/**
 * Array based Queue implementation.
 * 
 * @author Elvin Bearden
 * @version Created on: Feb 22, 2013
 *
 */
public class Queue<E> {
	/*
	 * Invariants:
	 * 1. nItems will hold the current number of items in the queue.
	 * 2. front will point to the next item to be removed.
	 * 3. rear will point to the location of the next added item.
	 * 4. The array data will hold the items from data[front] to data[rear]. 
	 */
	private int nItems;
	private int front;
	private int rear;
	private E[] data;
	
	/**
	 * Creates a new Queue of type E
	 */
	public Queue() {
		front = rear = nItems = 0;
		data = (E[])new Object[5];
	}
	
	/**
	 * Add an item to the front of the Queue.
	 * @param item		item to be added to the Queue.
	 * @precondition	item must be of type E.
	 * @postcondition	nItems and rear increase by one. The queue has one more item in it.
	 */
	public void add(E item) {
		if (nItems == data.length - 1) {
			expandArray();
		}
		data[rear] = item;
		rear = (rear + 1) % data.length;
		nItems++;
	}
	
	/**
	 * Remove the item at the front of the queue.
	 * @return	the item at the front of the queue.
	 */
	public E remove() {
		nItems--;
		E toReturn = data[front];
		front = (front + 1) % data.length;
		return toReturn;
	}
	
	/**
	 * Checks to see if the queue is empty
	 * @return	true if queue is empty, else false.
	 */
	public boolean isEmpty() {
		return (nItems == 0);
	}
	
	/**
	 * Size of the Queue.
	 * @return	The number of items in the Queue
	 */
	public int size() {
		return nItems;
	}
	
	/**
	 * Expands the array if the bounds are reached.
	 * Loosely based on example from book.
	 * @precondition	The queue has been instantiated.
	 * @postcondition	The array data is doubled.
	 */
	private void expandArray() {
		if (front <= rear) {
			E[] newArray = (E[])new Object[data.length * 2];
			System.arraycopy(data, front, newArray, front, nItems);
			data = newArray;
		}
		else {
			E[] newArray = (E[])new Object[data.length * 2];
			int n1 = data.length - front;
			int n2 = rear + 1;
			System.arraycopy(data, front, newArray, 0, n1);
			System.arraycopy(data, 0, newArray, n1, n2);
			front = 0;
			rear = nItems-1;  
			data = newArray;
			
		}
	}
	
	
	
	
}
