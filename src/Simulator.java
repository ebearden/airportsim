import edu.colorado.simulations.*;
/**
 * The simulator for the Airport Simulation Program.
 * 
 * @author Elvin Bearden
 * @version Created on: Feb 27, 2013
 *
 */
public class Simulator {
	private Queue<Integer> landingQueue;
	private Queue<Integer> takeOffQueue;
	private BooleanSource arrival;
	private BooleanSource departure;
	private Airport airport;
	private Averager landingWaitTimes;
	private Averager takeOffWaitTimes;
	private int maxWaitTime;
	private int totalSimTime;
	private double takeOffProb; 
	private double landingProb;
	private int currentTime;
	private int next;
	private int crashed;

	/**
	 * Constructor
	 * @param landingTime	time it takes for a plane to land
	 * @param takeOffTime	time it takes for a plane to takeoff
	 * @param takeOffProb	probability that a plane will want to takeoff in any given minute
	 * @param landingProb	probability that a plane will want to land in any given minute
	 * @param maxTime		maximum time a plane can wait to land before crashing
	 * @param totalSimTime	total simulation time.
	 */
	public Simulator(int landingTime, int takeOffTime, double takeOffProb, 
			double landingProb, int maxTime, int totalSimTime) {
		
		if (landingTime < 1 || takeOffTime < 1 || takeOffProb < 0 || takeOffProb > 1 ||
				landingProb < 0 || takeOffProb > 1 || maxTime < 1 || totalSimTime < 1) {
			throw new IllegalArgumentException("Values are out of range");
		}
		
		landingQueue = new Queue<Integer>();
		takeOffQueue = new Queue<Integer>();
		
		airport = new Airport(landingTime, takeOffTime);
		
		landingWaitTimes = new Averager();
		takeOffWaitTimes = new Averager();
		
		this.takeOffProb = takeOffProb;
		this.landingProb = landingProb;
		arrival = new BooleanSource(this.takeOffProb);
		departure = new BooleanSource(this.landingProb);
		
		maxWaitTime = maxTime;
		this.totalSimTime = totalSimTime;
		
		crashed = 0;
	}
	
	/**
	 * Runs the simulation.
	 */
	public void run() {		
		for (currentTime = 0; currentTime < totalSimTime; currentTime++) {
			if (arrival.query()) {
				landingQueue.add(currentTime);
			}
			
			if (departure.query()) {
				takeOffQueue.add(currentTime);
			}
			
			if (!airport.isBusy() && !landingQueue.isEmpty()) {
				next = landingQueue.remove();
				if (currentTime - next < maxWaitTime) {
					landingWaitTimes.addNumber(currentTime - next);
					airport.landPlane();
				}
				else {
					crashed++;
				}
			}
			
			if (!airport.isBusy() && !takeOffQueue.isEmpty() && landingQueue.isEmpty() ) {
				next = takeOffQueue.remove();
				takeOffWaitTimes.addNumber(currentTime - next);
				airport.planeTakeoff();
			}
			
			airport.reduceRemainingTime();	
		}
	}
	
	/**
	 * Get the inputs for the instance
	 * @return	the inputs for the simulation
	 */
	public String getInputs() {
		return 
		"Amount of time for one plane to land: " + airport.getLandingTime() + "\n" +
		"Amount of time for one plane to take off: " + airport.getTakeOffTime() + "\n" +
		"Probability that a plane wishes to take off during any given minute: " + takeOffProb + "\n" +
		"Probability that a plane wished to land during any given minute: " + landingProb + "\n" +
		"Maximum amount of time that a plane can stay in the landing queue without running out of fuel: " + maxWaitTime + "\n" +
		"Total simulation time: " + totalSimTime + "\n";
	}
	
	/**
	 * Gets the results for the instance
	 * @return	the results of the simulation
	 */
	public String getResults() {
		return
		"Number of planes that took off: " + takeOffWaitTimes.howManyNumbers() + "\n" +
		"Number of planes that landed: " + landingWaitTimes.howManyNumbers() + "\n" +
		"Number of planes that crashed: " + crashed + "\n" +
		"Average time a plane spent waiting to take off: " + takeOffWaitTimes.average() + "\n" +
		"Average time a plane spent waiting to land: " + landingWaitTimes.average() + "\n";
		 
		
	}
}
