/**
 * Airport Class: Simulates an Airport with one landing strip.
 * <dt>This class is based on Washer class from Michael Main.<dt>
 * <a href="http://www.cs.colorado.edu/~main/edu/colorado/simulations/Washer.java">Washer Class</a>
 * 
 * @author Elvin Bearden
 * @version Created on: Feb 15, 2013
 *
 */
public class Airport {
	private int timeToLand;
	private int timeToTakeoff;
	private int timeLeft;

	/**
	 * Constructor.
	 * @param timeToLand					Amount of time it takes one plane to land.
	 * @param timeToTakeoff					Amount of time it takes one plane to take-off.
	 * @throws IllegalArgumentException		If either input is less than 0.
	 */
	public Airport(int timeToLand, int timeToTakeoff) {
		if (timeToLand < 0 || timeToTakeoff < 0) {
			throw new IllegalArgumentException("Both inputs must be greater than zero.");
		}
		else {
			this.timeToLand = timeToLand;
			this.timeToTakeoff = timeToTakeoff;
		}
		timeLeft = 0;
	}
	
	/**
	 * Accessor
	 * @return	The time it takes one plane to land.
	 */
	public int getLandingTime() {
		return timeToLand;
	}
	
	/**
	 * Accessor
	 * @return	The time it takes one plane to takeoff.
	 */
	public int getTakeOffTime() {
		return timeToTakeoff;
	}
	
	/**
	 * Checks to see if there is a plane landing or taking-off.
	 *@return	True if there is an airplane landing or taking off, else false.
	 */
	public boolean isBusy() {
		return (timeLeft > 0);
	}
	
	/**
	 * Decrements timeLeft by one unit.
	 * @postcondition	timeLeft is one unit less.
	 */
	public void reduceRemainingTime() {
		if (timeLeft > 0) {
			timeLeft--;
		}
	}
	
	/**
	 * Starts a plane landing event.
	 * @postcondition	A timer is started for landing plane.
	 */
	public void landPlane() {
		if (isBusy()) {
			throw new IllegalStateException("Airport is busy");
		}
		else {
			timeLeft = timeToLand;
		}
	}
	
	/**
	 * Starts a plane take off event.
	 * @postcondition	A timer is started for a plane taking off.
	 */
	public void planeTakeoff() {
		if (isBusy()) {
			throw new IllegalStateException("Airport is busy");
		}
		else {
			timeLeft = timeToTakeoff;
		}
	}
}
