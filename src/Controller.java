/**
 * Controller Class for the Airport Simulator. 
 * Passes communications between View and "Model"
 * 
 * @author Elvin Bearden
 * @version Created on: Feb 27, 2013
 *
 */
public class Controller {
	Simulator simulator;
	View view;
	
	/**
	 * Starts the program
	 */
	public void start() {
		view = new View();
	}
	
	/**
	 * Instantiates a Simulator object then runs the simulation.
	 * @param integerInputs	The integer inputs from the view
	 * @param doubleInputs	The double inputs from the view
	 */
	public void startSim(int[] integerInputs, double[] doubleInputs) {
		simulator = new Simulator(integerInputs[0], integerInputs[1], 
				doubleInputs[0], doubleInputs[1], integerInputs[2], integerInputs[3]);
		
		simulator.run();
	}
	
	/**
	 * Gets the inputs for the current simulation
	 * @return	simulation inputs.
	 */
	public String getInputs() {
		return simulator.getInputs();
	}
	
	/**
	 * Gets the results for the current simulation
	 * @return	simulation results.
	 */
	public String getResults() {
		return simulator.getResults();
	}
}
