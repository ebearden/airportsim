import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * GUI for the simulation.
 * 
 * @author Elvin Bearden
 * @version Created on: Feb 27, 2013
 *
 */
public class View extends JFrame{
	private JPanel inputPanel;
	private JPanel displayPanel;
	private JPanel buttonPanel;
	
	private JLabel arrivalRateLbl;
	private JLabel departureRateLbl;
	private JLabel landingTimeLbl;
	private JLabel takeOffTimeLbl;
	private JLabel maxTimeLbl;
	private JLabel totalSimTimeLbl;
	
	private JTextField arrivalRateField;
	private JTextField departureRateField;
	private JTextField landingTimeField;
	private JTextField takeOffTimeField;
	private JTextField maxTimeField;
	private JTextField totalSimTimeField;
	
	private JButton runBtn;
	private JButton clearBtn;
	
	private JTextArea displayArea;
	private Controller controller;
	
	public View() {
		
		inputPanel = new JPanel(new GridLayout(7,2));
		
		arrivalRateLbl = new JLabel("Arrival Rate:");
		arrivalRateField = new JTextField(5);
		
		departureRateLbl = new JLabel("Departure Rate:");
		departureRateField = new JTextField(5);
		
		landingTimeLbl = new JLabel("Landing Time:");
		landingTimeField = new JTextField(5);
		
		takeOffTimeLbl = new JLabel("Takeoff Time:");
		takeOffTimeField = new JTextField(5);
		
		maxTimeLbl = new JLabel("Maximum Waiting Time:");
		maxTimeField = new JTextField(5);
		
		totalSimTimeLbl = new JLabel("Simulation Time:");
		totalSimTimeField = new JTextField(5);
		
		inputPanel.add(arrivalRateLbl);
		inputPanel.add(arrivalRateField);
		inputPanel.add(departureRateLbl);
		inputPanel.add(departureRateField);
		inputPanel.add(landingTimeLbl);
		inputPanel.add(landingTimeField);
		inputPanel.add(takeOffTimeLbl);
		inputPanel.add(takeOffTimeField);
		inputPanel.add(maxTimeLbl);
		inputPanel.add(maxTimeField);
		inputPanel.add(totalSimTimeLbl);
		inputPanel.add(totalSimTimeField);
		
		buttonPanel = new JPanel();
		runBtn = new JButton("Run");
		clearBtn = new JButton("Clear");
		buttonPanel.add(runBtn);
		buttonPanel.add(clearBtn);
		
		displayPanel = new JPanel();
		displayArea = new JTextArea(17,50);
		displayPanel.add(displayArea);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.CENTER);
		add(displayPanel, BorderLayout.SOUTH);
		
		
		controller = new Controller();
		
		runBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				int[] integerInputs = new int[4];
				double[] doubleInputs = new double[2];
				
				try {
					integerInputs[0] = Integer.parseInt(landingTimeField.getText());
					integerInputs[1] = Integer.parseInt(takeOffTimeField.getText());
					integerInputs[2] = Integer.parseInt(maxTimeField.getText());
					integerInputs[3] = Integer.parseInt(totalSimTimeField.getText());
					doubleInputs[0] = Double.parseDouble(arrivalRateField.getText());
					doubleInputs[1] = Double.parseDouble(departureRateField.getText());
					controller.startSim(integerInputs, doubleInputs);
					printToScreen(controller.getInputs() + "\n" + controller.getResults());
					//clearAllInputFields();
				} 
				catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, "Please enter correct data types");
				}
				catch (IllegalArgumentException ex) {
					JOptionPane.showMessageDialog(null, "Values are out of range");
				}	
				catch (NullPointerException ex) {
					JOptionPane.showMessageDialog(null, "Something went wrong hit run again.");
				}
			}
		});
		
		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearAllInputFields();
			}
		});
		
		setSize(600,500);
		setResizable(false);
		setVisible(true);
	}
	
	/**
	 * Prints to the display Area.
	 * @param input	What you want to print.
	 */
	private void printToScreen(String input) {
		displayArea.setText(input);
	}
	
	/**
	 * Clears all the input fields
	 * @postcondition	The input fields are cleared
	 */
	private void clearAllInputFields() {
		arrivalRateField.setText("");
		departureRateField.setText("");
		landingTimeField.setText("");
		takeOffTimeField.setText("");
		maxTimeField.setText("");
		totalSimTimeField.setText("");
	}
	
}
