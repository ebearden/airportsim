/**
 * Main class for airport simulation.
 * @author Elvin Bearden
 * @version Created on: Feb 15, 2013
 *
 */
public class AirportSim {
	public static void main(String[] args) {
		Controller controller = new Controller();
		controller.start();
	}
}
